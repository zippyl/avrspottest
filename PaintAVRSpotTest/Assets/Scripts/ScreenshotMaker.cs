﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotMaker : MonoBehaviour {

    [SerializeField] private Canvas canvas;

	public void MakeScreenshot()
    {
        StartCoroutine(Cheeeese());
    }

    private IEnumerator Cheeeese()
    {
        canvas.gameObject.SetActive(false);
        yield return new WaitForSeconds(.2f);
        ScreenCapture.CaptureScreenshot("familyPhoto.png");
        yield return new WaitForSeconds(.2f);
        canvas.gameObject.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToolPicker : MonoBehaviour
{

    public DrawLineScript.ToolPick toolPick;
    private DrawLineScript drawScript;

    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => { drawScript = FindObjectOfType<DrawLineScript>(); drawScript.SetTool(toolPick); Debug.Log(toolPick); });
    }
}

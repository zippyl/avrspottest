﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DrawLineScript : MonoBehaviour
{

    public enum ColorPick { Red, Green, Blue, Yellow, Cyan }
    public enum ToolPick { Draw, Erase, CustomBrush }
    [SerializeField] private ColorPick colorPick;
    [SerializeField] private Slider sizeRange;
    [SerializeField] private GameObject lineRendererPrefab;
    [SerializeField] private Material customMaterial;

    private float size;
    private int sortPosition = 0;
    private bool canCreateLineRenderer = true;
    private Color color;
    private ToolPick tool;
    private List<Vector3> points;
    public List<GameObject> lineRenderersGameObject;

    // Use this for initialization
    void Start()
    {
        points = new List<Vector3>();
        lineRenderersGameObject = new List<GameObject>();
        SetColor(ColorPick.Red);
        SetTool(ToolPick.Draw);
        sizeRange.onValueChanged.AddListener(delegate { size = sizeRange.value; });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (tool == ToolPick.Draw || tool == ToolPick.CustomBrush)
                    DrawLine();
                if (tool == ToolPick.Erase)
                    EraseLine();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            canCreateLineRenderer = true;
        }
    }

    public void SetColor(ColorPick colorPick)
    {
        switch (colorPick)
        {
            case ColorPick.Red: color = Color.red; break;
            case ColorPick.Green: color = Color.green; break;
            case ColorPick.Blue: color = Color.blue; break;
            case ColorPick.Yellow: color = Color.yellow; break;
            case ColorPick.Cyan: color = Color.cyan; break;
        }
    }

    public void SetTool(ToolPick tool)
    {
        switch (tool)
        {
            case ToolPick.Draw: this.tool = ToolPick.Draw; break;
            case ToolPick.Erase: this.tool = ToolPick.Erase; break;
            case ToolPick.CustomBrush: this.tool = ToolPick.CustomBrush; break;
        }
    }

    private void DrawLine()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (!points.Contains(mousePos))
        {
            CreateLineRenderer();
            points.Add(mousePos);
            mousePos.z = LastLineRenderer.transform.position.z;
            LastLineRenderer.positionCount = points.Count;
            LastLineRenderer.startColor = color;
            LastLineRenderer.endColor = color;
            LastLineRenderer.startWidth = size;
            LastLineRenderer.endWidth = size;
            LastLineRenderer.SetPosition(LastLineRenderer.positionCount - 1, mousePos);
        }
    }

    private void EraseLine()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (!points.Contains(mousePos))
        {
            CreateLineRenderer();
            points.Add(mousePos);
            mousePos.z = LastLineRenderer.transform.position.z;
            LastLineRenderer.positionCount = points.Count;
            LastLineRenderer.startColor = Color.white;
            LastLineRenderer.endColor = Color.white;
            LastLineRenderer.startWidth = size;
            LastLineRenderer.endWidth = size;
            LastLineRenderer.SetPosition(LastLineRenderer.positionCount - 1, mousePos);
        }
    }

    private void CreateLineRenderer()
    {
        if (canCreateLineRenderer)
        {
            sortPosition++;
            canCreateLineRenderer = false;
            size = sizeRange.value;
            var lineRenderer = Instantiate(lineRendererPrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
            if (tool == ToolPick.Draw)
                lineRenderer.GetComponent<LineRenderer>().material = new Material(Shader.Find("Particles/Alpha Blended"));
            if (tool == ToolPick.CustomBrush)
                lineRenderer.GetComponent<LineRenderer>().material = customMaterial;
            lineRenderer.GetComponent<LineRenderer>().useWorldSpace = true;
            lineRenderer.GetComponent<LineRenderer>().sortingOrder = sortPosition;
            lineRenderer.transform.SetAsFirstSibling();
            lineRenderersGameObject.Add(lineRenderer);
        }
    }

    private LineRenderer LastLineRenderer { get { return lineRenderersGameObject[lineRenderersGameObject.Count - 1].GetComponent<LineRenderer>(); } }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonColorPicker : MonoBehaviour
{

    public DrawLineScript.ColorPick colorPick;
    private DrawLineScript drawScript;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => { drawScript = FindObjectOfType<DrawLineScript>(); drawScript.SetColor(colorPick); Debug.Log(colorPick); });
    }

}
